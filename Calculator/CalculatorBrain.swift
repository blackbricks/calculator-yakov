//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by yakov shteffen on 18/02/2018.
//  Copyright © 2018 yakov shteffen. All rights reserved.
//

import Foundation

var variableDictionary = Dictionary<CalculatorBrain.VariableName, Double>()

struct CalculatorBrain {
    
    //PropertyList (computed property "program" is used to store calculation program in userDefaults)
    typealias PropertyList = AnyObject
    
    var program: PropertyList {
        get {
            var propertyListProgram = [Any]()
            for op in calculationProgram {
                switch op {
                case .operand(let operand):
                    propertyListProgram.append(operand as Any)
                case .operation(let symbol):
                    propertyListProgram.append(symbol as Any)
                case .variable(let varName):
                    propertyListProgram.append(varName.rawValue as Any)
                }
            }
            return propertyListProgram as PropertyList
        }
        set {
            clear()
            if let arrayOfAny = newValue as? [Any] {
                for op in arrayOfAny {
                    if let operand = op as? Double {
                        calculationProgram.append(CalculationElement.operand(operand))
                    } else if let symbol = op as? String {
                        if operations[symbol] != nil {
                            calculationProgram.append(CalculationElement.operation(symbol))
                        } else {
                            calculationProgram.append(CalculationElement.variable(VariableName(rawValue: symbol)!))
                        }
                    }
                }
            }
        }
    }
    //
    
    
    var resultIsPending: Bool {
        return evaluate().isPending
    }
    
    var result: Double? {
        return evaluate().result
    }
    
    var errorDescription: String? {
        return evaluate().errorDescription
    }
    
    mutating func performOperation(_ symbol: String) {
        setOperand(symbol)
    }
    
    func getDescription() -> String {
        return evaluate().description
    }
    
    mutating func setOperand(variable named: VariableName) {
        calculationProgram.append(CalculationElement.variable(named))
    }
    
    mutating func setOperand(_ operand: Double) {
        calculationProgram.append(CalculationElement.operand(operand))
    }
    
    mutating func undo() {
        if !calculationProgram.isEmpty {
            calculationProgram.removeLast()
        }
    }
    
    mutating func clear() {
        calculationProgram.removeAll()
    }
    
    func evaluate(using variables: Dictionary<VariableName,Double>? = nil)
        -> (result: Double?, isPending: Bool, description: String, errorDescription: String?) {
            var data = CalculatorData(
                accumulator: nil,
                description: " ",
                errorDescription: nil,
                resultIsPending: false,
                pendingBinaryOperation: nil
            )
            if variables != nil {
                variableDictionary.merge(variables!, uniquingKeysWith: { (_, new) in new })
            }
            let calculationProgram = self.calculationProgram
            if calculationProgram.isEmpty {
                return (nil, false, " ", nil)
            }
            for element in calculationProgram {
                switch element {
                case .operand(let value):
                    data = evaluateOperand(using: data, from: value)
                case .operation(let symbol):
                    data = evaluateOperation(using: data, operation: symbol)
                case .variable(let varName):
                    data = evaluateVariable(using: data, variable: varName)
                }
            }
            if data.accumulator != nil {
                if data.accumulator!.isNaN {
                    data.errorDescription = "Error. Result is not a number"
                }
                if data.accumulator!.isInfinite {
                    data.errorDescription = "Error. Result is infinite"
                }
            }
            return (data.accumulator, data.resultIsPending, data.description, data.errorDescription)
    }
    
    //contains all supported names for variables
    enum VariableName: String {
        case M = "M"
        case x = "x"
        case y = "y"
        case z = "z"
    }
    
    //Private part
    
    private var calculationProgram = Array<CalculationElement>()
    
    private enum Operation {
        case constant(Double)
        case unaryOperation((Double) -> Double)
        case binaryOperation((Double, Double) -> Double)
        case noArgOperation(() -> Double)
        case equals
        case reset
    }
    
    private var operations: Dictionary<String,Operation> = [
        "π" : Operation.constant(Double.pi),
        "e" : Operation.constant(M_E),
        "√" : Operation.unaryOperation(sqrt),
        "x²" : Operation.unaryOperation({ $0 * $0 }),
        "sin" : Operation.unaryOperation(sin),
        "cos" : Operation.unaryOperation(cos),
        "tan" : Operation.unaryOperation(tan),
        "±" : Operation.unaryOperation({ -$0 }),
        "-" : Operation.binaryOperation({ $0 - $1 }),
        "+" : Operation.binaryOperation({ $0 + $1 }),
        "÷" : Operation.binaryOperation({ $0 / $1 }),
        "×" : Operation.binaryOperation({ $0 * $1 }),
        "xʸ" : Operation.binaryOperation({ pow($0, $1) }),
        "rand" : Operation.noArgOperation({ Double(drand48()) }),
        "=" : Operation.equals,
        "C" : Operation.reset
    ]
    
    private struct PendingBinaryOperation {
        let function: (Double, Double) -> Double
        let firstOperand: Double
        
        func perform(with secondOperand: Double) -> Double {
            return function(firstOperand, secondOperand)
        }
    }
    
    private enum CalculationElement {
        case operand(Double)
        case operation(String)
        case variable(VariableName)
    }
    
    private struct CalculatorData {
        var accumulator: Double?
        var description: String
        var errorDescription: String?
        var resultIsPending: Bool
        var pendingBinaryOperation: PendingBinaryOperation?
    }
    
    private func evaluateOperand(using current: CalculatorData, from value: Double) -> CalculatorData {
        var data = current
        data.accumulator = value
        if !data.resultIsPending {
            data.description = formatMyNumber(data.accumulator!)
        } else {
            data.description += formatMyNumber(data.accumulator!)
        }
        return data
    }
    
    private func evaluateVariable(using current: CalculatorData, variable: VariableName) -> CalculatorData {
        var data = current
        if data.resultIsPending {
            data.description += "\(variable)"
        } else {
            data.description = "\(variable)"
        }
        if !variableDictionary.isEmpty {
            data.accumulator = variableDictionary[variable] ?? 0
        } else {
            data.accumulator = 0
        }
        return data
    }
    
    private func evaluateOperation(using current: CalculatorData, operation: String) -> CalculatorData {
        var data = current
        let symbol = operation
        if let operation = operations[symbol] {
            switch operation {
            case .constant(let value):
                data = evaluateConstant(using: data, operation: symbol, constant: value)
            case .unaryOperation(let function):
                data = evaluateUnaryOp(using: data, operation: symbol, with: function)
            case .noArgOperation(let function):
                data = evaluateNoArgOp(using: data, with: function)
            case .binaryOperation(let function):
                data = evaluateBinaryOp(using: data, operation: symbol, with: function)
            case .equals:
                data = evaluateEquals(using: data)
            case .reset:
                data.accumulator = nil
                data.pendingBinaryOperation = nil
                data.description = " "
                variableDictionary.removeAll()
            }
        }
        return data
    }
    
    private func evaluateConstant(using current: CalculatorData, operation: String, constant: Double) -> CalculatorData {
        var data = current
        let symbol = operation
        data.accumulator = constant
        if data.resultIsPending {
            data.description += symbol
        } else {
            data.description = symbol
        }
        return data
    }
    
    private func evaluateUnaryOp(using current: CalculatorData, operation: String, with function: (Double) -> Double) -> CalculatorData {
        var data = current
        let symbol = operation
        if data.accumulator != nil {
            if data.resultIsPending {
                //remove last element in description, then put it back with correct formatting e.g. √(x)
                data.description.removeLast(formatMyNumber(data.accumulator!).count)
                data.description += "\(symbol)(\(formatMyNumber(data.accumulator!)))"
                data.accumulator = function(data.accumulator!)
            } else {
                data.description = "\(symbol)(\(data.description))"
                data.accumulator = function(data.accumulator!)
            }
        }
        return data
    }
    
    private func evaluateNoArgOp(using current: CalculatorData, with function: () -> Double) -> CalculatorData {
        var data = current
        data.accumulator = function()
        if data.resultIsPending {
            data.description += formatMyNumber(data.accumulator!)
        } else {
            data.description = formatMyNumber(data.accumulator!)
        }
        return data
    }
    
    private func evaluateBinaryOp(using current: CalculatorData, operation: String, with function: @escaping (Double, Double) -> Double) -> CalculatorData {
        var data = current
        let symbol = operation
        if data.accumulator != nil {
            data.description += " \(symbol) "
            data.resultIsPending = true
            //called here to allow for operation chains like "2 + 2 + 4"
            //performPendingBinaryOperation()
            if data.pendingBinaryOperation != nil && data.accumulator != nil {
                data.accumulator = data.pendingBinaryOperation!.perform(with: data.accumulator!)
                data.pendingBinaryOperation = nil
            }
            
            data.pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: data.accumulator!)
            data.accumulator = nil
        }
        return data
    }
    
    private func evaluateEquals(using current: CalculatorData) -> CalculatorData {
        var data = current
        //prevents buggy state when user enters number, presses binary operation and "=" before entering second operand
        if data.accumulator != nil {
            //performPendingBinaryOperation()
            if data.pendingBinaryOperation != nil && data.accumulator != nil {
                data.accumulator = data.pendingBinaryOperation!.perform(with: data.accumulator!)
                data.pendingBinaryOperation = nil
            }
            data.resultIsPending = false
        }
        return data
    }
    
    private func formatMyNumber(_ value: Double) -> String {
        let localFormatter = NumberFormatter()
        localFormatter.minimumIntegerDigits = 1
        localFormatter.maximumFractionDigits = 6
        return localFormatter.string(from: NSNumber.init(value: value))!
    }
    
    private mutating func setOperand(_ symbol: String) {
        calculationProgram.append(CalculationElement.operation(symbol))
    }  
}
