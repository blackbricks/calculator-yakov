//
//  ViewController.swift
//  Calculator
//
//  Created by yakov shteffen on 14/02/2018.
//  Copyright © 2018 yakov shteffen. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController, UISplitViewControllerDelegate {
    typealias PropertyList = AnyObject
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var operationsDisplay: UILabel!
    @IBOutlet weak var memoryDisplay: UILabel!
    @IBOutlet weak var graphButton: UIButton! {
       didSet {
        graphButton.isEnabled = false
        graphButton.alpha = 0.25
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return brain.evaluate().description.contains(CalculatorBrain.VariableName.M.rawValue) && !brain.evaluate().isPending
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationViewController = segue.destination
        if let navigationController = destinationViewController as? UINavigationController {
            destinationViewController = navigationController.visibleViewController ?? destinationViewController
        }
        if let grapherViewController = destinationViewController as? GrapherViewController {
            grapherViewController.yForX = { [weak self] x in
                self?.variableDictionary[.M] = x
                return self?.brain.evaluate(using: self?.variableDictionary).result
            }
            grapherViewController.navigationItem.title = brain.evaluate().description
        }
        program = brain.program
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let savedProgram = program as? [Any] {
            brain.program = savedProgram as PropertyList
            operationsDisplay.text = brain.evaluate().description
            performSegue(withIdentifier: "graph", sender: nil)
        }
    }
    
    @IBAction func touchBackspace(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            if display.text!.count > 1 {
                display.text!.removeLast()
            } else if display.text!.count == 1 {
                display.text! = "0"
                userIsInTheMiddleOfTyping = false
            }
        } else {
            brain.undo()
            if brain.resultIsPending {
                operationsDisplay.text = brain.getDescription() + " ..."
            } else {
                operationsDisplay.text = brain.getDescription() + " ="
            }
        }
    }
    
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            if digit == "." {
                //adding "." is possible only if there are no decimal separators in display
                if !textCurrentlyInDisplay.contains(".") {
                    display.text = textCurrentlyInDisplay + digit
                }
            } else {
                display.text = textCurrentlyInDisplay + digit
            }
        } else {
            //special case if user presses "." before entering any numbers
            if digit == "." {
                display.text = "0."
            } else {
                display.text = digit
            }
            userIsInTheMiddleOfTyping = true
        }
    }
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            changeGraphButton()
            displayFormatter.minimumIntegerDigits = 1
            displayFormatter.maximumFractionDigits = 6
            display.text = displayFormatter.string(from: NSNumber.init(value: newValue))
        }
    }
    
    @IBAction func performMemoryOperation(_ sender: UIButton) {
        userIsInTheMiddleOfTyping = false
        let action = sender.currentTitle
        if action == "M" {
            brain.setOperand(variable: .M)
            operationsDisplay.text = brain.getDescription() + " ..."
        } else {
            //action == →M
            memoryDisplay.text = "M=" + display.text!
            variableDictionary[.M] = displayValue
            displayValue = brain.evaluate(using: variableDictionary).result ?? 0
            operationsDisplay.text = brain.getDescription() + " ="
        }
    }
    
    @IBAction func performOperation(_ sender: UIButton) {
        
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
            if brain.resultIsPending {
                operationsDisplay.text = brain.getDescription() + " ..."
            } else {
                operationsDisplay.text = brain.getDescription() + " ="
            }
            //NaN and +/- infinity errors
            if let errDesc = brain.errorDescription {
                operationsDisplay.text = errDesc
            }
        }
        if let result = brain.result {
            displayValue = result
        }
        if sender.currentTitle != nil && sender.currentTitle == "C" {
            display.text = "0"
            operationsDisplay.text = " "
            userIsInTheMiddleOfTyping = false
            memoryDisplay.text = "M="
            brain.clear()
            program = nil
            variableDictionary = [:]
            changeGraphButton()
        }
    }
    
    private func changeGraphButton() {
        if brain.evaluate().description.contains(CalculatorBrain.VariableName.M.rawValue) && !brain.evaluate().isPending {
            graphButton.isEnabled = true
            graphButton.alpha = 1.0
        } else {
            graphButton.isEnabled = false
            graphButton.alpha = 0.25
        }
    }
    
    // forcing SplitViewController to show master view instead of graph at launch
    override func awakeFromNib() {
        super.awakeFromNib()
        splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool {
        return true
    }
    
    private var program: PropertyList? {
        get {
            return defaults.object(forKey: "calcProgram") as PropertyList?
        }
        set {
            defaults.set(newValue, forKey: "calcProgram")
        }
    }
    
    private let defaults = UserDefaults.standard
    private let displayFormatter = NumberFormatter()
    private var userIsInTheMiddleOfTyping = false
    private var brain = CalculatorBrain()
    private var variableDictionary: Dictionary<CalculatorBrain.VariableName, Double> = [:]
}

