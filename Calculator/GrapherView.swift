//
//  GrapherView.swift
//  grapher mvc
//
//  Created by yakov shteffen on 27/02/2018.
//  Copyright © 2018 yakov shteffen. All rights reserved.
//

import UIKit

@IBDesignable
class GrapherView: UIView {
    
    @IBInspectable
    //40 is something like "1 to 1, changes with pinching gesture"
    var ppiFactor: CGFloat = 40 {
        didSet {
            setNeedsDisplay()
            UserDefaults.standard.set(Double(ppiFactor), forKey: "grapherScale")
        }
    }
    var originPoint: CGPoint? = nil {
        didSet {
            setNeedsDisplay()
            let originX = Double(originPoint!.x)
            let originY = Double(originPoint!.y)
            UserDefaults.standard.set(originX, forKey: "originX")
            UserDefaults.standard.set(originY, forKey: "originY")
        }
    }
    
    func clear() {
        graphPath.removeAllPoints()
    }
    
    func setAxisOrigin(to point: CGPoint) {
        originPoint = point
    }
    
    func moveAxisOrigin(using translation: CGPoint) {
        let point = originPoint ?? CGPoint(x: bounds.midX, y: bounds.midY)
        originPoint = CGPoint(x: point.x + translation.x, y: point.y + translation.y)
    }
    
    func prepare(startingPoint: CGPoint) {
        graphPath.move(to: calculateCoordinates(x: startingPoint.x , y: startingPoint.y))
    }
    
    func drawToNext(point: CGPoint) {
        graphPath.addLine(to: calculateCoordinates(x: point.x , y: point.y))
    }
    
    override func draw(_ rect: CGRect) {
        let axes = AxesDrawer(color: axisColor, contentScaleFactor: CGFloat(1))
        if originPoint != nil {
            axes.drawAxes(in: rect, origin: CGPoint(x: originPoint!.x, y: originPoint!.y), pointsPerUnit: ppiFactor)
        } else {
            axes.drawAxes(in: rect, origin: CGPoint(x: bounds.midX, y: bounds.midY), pointsPerUnit: ppiFactor)
        }
        graphPath.lineWidth = graphLineWidth
        graphColor.set()
        graphPath.stroke()
    }
    
    private func calculateCoordinates(x: CGFloat, y: CGFloat) -> CGPoint {
        if originPoint != nil {
            return CGPoint(x: originPoint!.x + x * ppiFactor, y: originPoint!.y - y * ppiFactor)
        } else {
            return CGPoint(x: bounds.midX + x * ppiFactor, y: bounds.midY - y * ppiFactor)
        }
    }
    private var axisColor: UIColor = UIColor.black
    private var graphColor: UIColor = UIColor.blue
    private var graphLineWidth: CGFloat = 2.0
    private var axes = AxesDrawer(color: UIColor.blue, contentScaleFactor: CGFloat(1.0))
    private let graphPath = UIBezierPath()
}
