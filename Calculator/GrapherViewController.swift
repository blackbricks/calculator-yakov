//
//  ViewController.swift
//  grapher mvc
//
//  Created by yakov shteffen on 27/02/2018.
//  Copyright © 2018 yakov shteffen. All rights reserved.
//

import UIKit

class GrapherViewController: UIViewController {
    
    var yForX: ((Double) -> Double?)!
    var minX: Double = -30
    var maxX: Double = 30
    var graphStep: Double = 0.05
    var justRotated: Bool = false
    var savedOriginPoint = CGPoint()
    var originOffcet = CGPoint()
    private var snapshot: UIView!
    
    @IBOutlet weak var grapherView: GrapherView! {
        didSet {
            let pinchHandler = #selector(changeScale(byReactingTo:))
            let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: pinchHandler)
            let panHandler = #selector(panningMove(byReactingTo:))
            let panRecognizer = UIPanGestureRecognizer(target: self, action: panHandler)
            let tapHandler = #selector(handleTap(byReactingTo:))
            let tapRecognizer = UITapGestureRecognizer(target: self, action: tapHandler)
            tapRecognizer.numberOfTapsRequired = 2
            grapherView.addGestureRecognizer(pinchRecognizer)
            grapherView.addGestureRecognizer(panRecognizer)
            grapherView.addGestureRecognizer(tapRecognizer)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let point = grapherView.originPoint {
            originOffcet = CGPoint(x: grapherView.bounds.midX - point.x,
                                   y: grapherView.bounds.midY - point.y)
        }
        justRotated = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let savedScale = UserDefaults.standard.double(forKey: "grapherScale")
        let scale = (savedScale == 0) ? 40 : savedScale
        grapherView.ppiFactor = CGFloat(scale)
        let savedOriginX = UserDefaults.standard.double(forKey: "originX")
        let savedOriginY = UserDefaults.standard.double(forKey: "originY")
        if justRotated {
            savedOriginPoint = CGPoint(x: grapherView.bounds.midX - originOffcet.x,
                                       y: grapherView.bounds.midY - originOffcet.y)
            justRotated = false
        } else {
            savedOriginPoint = (savedOriginX == 0 && savedOriginY == 0)
                ? CGPoint(x: grapherView.bounds.midX, y: grapherView.bounds.midY)
                : CGPoint(x: savedOriginX, y: savedOriginY)
        }
        grapherView.originPoint = savedOriginPoint
        calculateDraw()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(Double(grapherView.ppiFactor), forKey: "grapherScale")
    }
    
    @objc
    func panningMove (byReactingTo panRecognizer: UIPanGestureRecognizer) {
        switch panRecognizer.state {
        case .began:
            snapshot = grapherView.snapshotView(afterScreenUpdates: false)
            snapshot!.alpha = 0.6
            grapherView.addSubview(snapshot!)
        case .changed:
            let translation = panRecognizer.translation(in: grapherView)
            snapshot!.center.x += translation.x
            snapshot!.center.y += translation.y
            panRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: grapherView)
        case .ended:
            grapherView.moveAxisOrigin(using: CGPoint(x: snapshot!.frame.origin.x, y: snapshot!.frame.origin.y))
            calculateDraw()
            snapshot!.removeFromSuperview()
            snapshot = nil
        default:
            break
        }
    }
    
    @objc
    func changeScale (byReactingTo pinchRecognizer: UIPinchGestureRecognizer) {
        switch pinchRecognizer.state {
        case .began:
            snapshot = grapherView.snapshotView(afterScreenUpdates: false)
            snapshot!.alpha = 0.8
            grapherView.addSubview(snapshot!)
        case .changed:
            let touch = grapherView.originPoint!
            snapshot!.frame.size.height *= pinchRecognizer.scale
            snapshot!.frame.size.width *= pinchRecognizer.scale
            //next 2 lines keep snapshot in center of the pinch
            snapshot!.frame.origin.x = snapshot!.frame.origin.x * pinchRecognizer.scale + (1 - pinchRecognizer.scale) * touch.x
            snapshot!.frame.origin.y = snapshot!.frame.origin.y * pinchRecognizer.scale + (1 - pinchRecognizer.scale) * touch.y
            pinchRecognizer.scale = 1
        case .ended:
            let changedScale = snapshot!.frame.height / grapherView.frame.height
            grapherView.ppiFactor *= changedScale
            snapshot!.removeFromSuperview()
            snapshot = nil
            calculateDraw()
        default:
            break
        }
    }
    
    @objc
    func handleTap (byReactingTo tapRecognizer: UITapGestureRecognizer) {
        if tapRecognizer.state == .ended {
            grapherView.setAxisOrigin(to: tapRecognizer.location(in: grapherView))
            calculateDraw()
        }
    }
    
    func calculateDraw() {
        if yForX != nil {
            let start = CGPoint(x: minX, y: yForX(minX)!)
            grapherView.clear()
            grapherView.prepare(startingPoint: start)
            for xCoord in stride(from: minX, through: maxX, by: graphStep) {
                let next = CGPoint(x: xCoord, y: yForX(xCoord)!)
                grapherView.drawToNext(point: next)
            }
        }
    }
}

