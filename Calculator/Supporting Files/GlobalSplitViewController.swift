//
//  GlobalSplitViewController.swift
//  Calculator
//
//  Created by yakov shteffen on 06/03/2018.
//  Copyright © 2018 yakov shteffen. All rights reserved.
//

import Foundation
import UIKit

class GlobalSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
    }
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
        return false
    }
    
}
